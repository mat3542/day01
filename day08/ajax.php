<?php
include("database.php");

$departments = array('MAT' => 'Khoa học máy tính', 'KDL' => 'Khoa học vật liệu');

if (isset($_POST['department']) && isset($_POST['keyword'])) {
    // Tiếp tục xử lý
    $stmt = $conn->prepare("SELECT * FROM students WHERE students.department LIKE :department AND students.name LIKE :keyword");
    $stmt->bindValue(':department', '%' . $_POST['department'] . '%', PDO::PARAM_STR);
    $stmt->bindValue(':keyword', '%' . $_POST['keyword'] . '%', PDO::PARAM_STR);
    $stmt->execute();

    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $html = "";
    foreach ($result as $key => $value) {
        $html .= "
            <tr class='mb-3'>
                <td>{$value['id']}</td>
                <td>{$value['name']}</td>
                <td>{$departments[$value['department']]}</td>
                <td class='d-flex justify-content-center'>
                    <button class='me-2 db-btn'>Xóa</button>
                    <button class='db-btn'>Sửa</button>
                </td>
            </tr>";
    }

    // Đảm bảo rằng dữ liệu được trả về là một đối tượng JSON hợp lệ
    header('Content-Type: application/json');

    // Gửi kết quả về client dưới dạng JSON
    echo json_encode(
        array("status" => "Success", "data" => $html)
    );
} else {
   return error;
}
?>
