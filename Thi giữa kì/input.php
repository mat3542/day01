<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form đăng ký sinh viên</title>
    <style>
        body {
            max-width: 700px;
            margin: 0 auto;
            padding: 20px;
            border: 2px solid #007bff;
            border-radius: 10px;
            background-color: #fff;
            text-align: center;
        }

        #error_messages {
            color: #f00;
            margin-bottom: 20px;
        }

        label {
            font-weight: bold;
            color: #fff;
            background-color: #888;
            padding: 6px;
            display: inline-block;
            width: 110px;
        }

        .form_container {
            text-align: left;
        }

        .form_section {
            margin-bottom: 20px;
        }

        input[type="text"],
        select,
        input[type="file"] {
            height: 27px;
            border: 1px solid #007bff;
        }

        .gender {
            display: flex;
        }

        .button_container {
            text-align: center;
        }

        #additional_info {
            height: 40px;
        }

        #register_button {
            background-color: #888;
            color: #fff;
            padding: 10px 20px;
            border: 2px solid #888;
            cursor: pointer;
            border-radius: 5px;
        }
    </style>
</head>

<body>
    <h1>Form đăng ký sinh viên</h1>

    <div id="error_messages"></div>

    <div class="form_container">
        <form id="registration_form" method="post" action="regist_student.php">
            <div class="form_section">
                <label for="full_name">Họ và Tên</label>
                <input type="text" id="full_name" name="full_name" required>
                <span id="fname-validation" class="error"></span>
            </div>

            <div class="form_section gender">
                <label>Giới tính</label>
                <input type="radio" id="gender_male" name="gender" value="Nam" required> Nam
                <input type="radio" id="gender_female" name="gender" value="Nữ" required> Nữ
            </div>

            <div class="form_section">
                <label for="birth">Ngày sinh</label>

                <select id="birth_year" name="birth_year" required>
                    <option value="" disabled selected>Năm</option>
                    <?php for ($year = date("Y") - 50; $year <= date("Y"); $year++): ?>
                        <option value="<?= $year ?>">
                            <?= $year ?>
                        </option>
                    <?php endfor; ?>
                </select>


                <select id="birth_month" name="birth_month" required>
                    <option value="" disabled selected>Tháng</option>
                    <?php for ($month = 1; $month <= 12; $month++): ?>
                        <option value="<?= $month ?>">
                            <?= $month ?>
                        </option>
                    <?php endfor; ?>
                </select>


                <select id="birth_day" name="birth_day" required>
                    <option value="" disabled selected>Ngày</option>
                    <?php for ($day = 1; $day <= 31; $day++): ?>
                        <option value="<?= $day ?>">
                            <?= $day ?>
                        </option>
                    <?php endfor; ?>
                </select>

                <span id="birth-validation" class="error"></span>
            </div>

            <div class="form_section">
                <label for="address">Địa chỉ</label>
                <select id="TP" name="TP" onchange="loadDistricts()">
                    <option value="" select disable>Thành phố</option>
                    <option value="Hanoi">Hà Nội</option>
                    <option value="TPHCM">Tp.Hồ Chí Minh</option>
                </select>
                <select id="district" name="district">
                    <option value="" select disable>Quận/Huyện</option>
                </select>
                <span id="address-validation" class="error"></span>
            </div>

            <div class="form_section">
                <label for="additional_info">Thông tin khác</label>
                <textarea id="additional_info" name="additional_info"></textarea>
            </div>

            <div class="button_container">
                <button type="button" id="register_button">Đăng ký</button>
            </div>
        </form>
        <div id="error_messages"></div>
    </div>

    <script>
        var registerButton = document.getElementById("register_button");
        var fullName = document.getElementById("full_name");
        var genderMale = document.getElementById("gender_male");
        var genderFemale = document.getElementById("gender_female");
        var birthDay = document.getElementById("birth_day");
        var birthMonth = document.getElementById("birth_month");
        var birthYear = document.getElementById("birth_year");
        var TPSelect = document.getElementById("TP");
        var districtSelect = document.getElementById("district");
        var birthValidation = document.getElementById("birth-validation");
        var errorMessage = document.getElementById('error_messages');

        errorMessage.innerHTML = '';

        function loadDistricts() {
            var districts = {
                "Hanoi": ["Ba Đình", "Hoàn Kiếm", "Hai Bà Trưng", "Đống Đa", "Tây Hồ", "Cầu Giấy"],
                "TPHCM": ["Quận 1", "Quận 2", "Quận 3", "Quận 4", "Quận 5"]
            };

            // Reset district options
            districtSelect.innerHTML = '<option value="" disabled selected>Quận</option>';

            var selectedCity = TPSelect.value;
            if (selectedCity in districts) {
                districts[selectedCity].forEach(function (district) {
                    var option = document.createElement("option");
                    option.value = district;
                    option.text = district;
                    districtSelect.appendChild(option);
                });
            }
        }

        registerButton.addEventListener("click", function (event) {
            event.preventDefault();

            // Validation
            var valid = true;

            if (fullName.value.trim() === "") {
                errorMessage.innerHTML += 'Hãy nhập họ tên.<br>';
                valid = false;
            }

            if (!genderMale.checked && !genderFemale.checked) {
                errorMessage.innerHTML += 'Hãy chọn giới tính <br>';
                valid = false;
            }

            if (birthDay.value === "" || birthMonth.value === "" || birthYear.value === "") {
                errorMessage.innerHTML += "Hãy chọn ngày sinh <br>";
                valid = false;
            } else {
                birthValidation.innerHTML = "";
            }

            if (TPSelect.value === "" || districtSelect.value === "") {
                errorMessage.innerHTML += "Hãy chọn địa chỉ";
                valid = false;
            }

            if (valid) {
                // Proceed with form submission
                document.getElementById("registration_form").submit();
            }
        });

    </script>
</body>

</html>