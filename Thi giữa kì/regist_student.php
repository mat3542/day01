<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Xác nhận thông tin đăng ký</title>
</head>

<style>
    body {
        max-width: 800px;
        margin: 0 auto;
        padding: 20px;
        border: 2px solid #888;
        border-radius: 10px;
        background-color: #fff;
        text-align: center;
    }

    .form_container {
        text-align: left;
    }

    strong {
        font-weight: bold;
        color: #fff;
        background-color: #888;
        padding: 3px;
        margin-right: 30px;
        display: inline-block;
        width: 100px;
    }

    #confirm-button {
        background-color: #888;
        color: #fff;
        padding: 10px 20px;
        border: 1px solid #007bff;
        cursor: pointer;
        border-radius: 5px;
    }
</style>

<body>
    <div class="form_container">
        <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $full_name = $_POST["full_name"];
            $gender = $_POST["gender"];
            $birth_day = $_POST["birth_day"];
            $birth_month = $_POST["birth_month"];
            $birth_year = $_POST["birth_year"];
            $address = $_POST["district"] . ' - ' . $_POST["TP"];
            $additional_info = $_POST["additional_info"];

            $birth = date("d/m/Y", strtotime("$birth_year-$birth_month-$birth_day"));

            echo "<p><strong>Họ và Tên:</strong> $full_name</p>";
            echo "<p><strong>Giới tính:</strong> $gender</p>";
            echo "<p><strong>Ngày sinh:</strong> $birth</p>";
            echo "<p><strong>Địa chỉ:</strong> $address</p>";
            echo "<p><strong>Thông tin khác:</strong> $additional_info</p>";
        }
        ?>
    </div>
</body>

</html>
