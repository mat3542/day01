<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <title>Đăng Ký</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/date-fns@2.24.0/esm/index.js"></script>


</head>
<style>
  * {
    box-sizing: border-box;
    border: none;
    outline: unset;
    font-size: 16px;
    font-family: "Times New Roman", Times, serif;
  }

  body {
    display: flex;
    align-items: center;
    justify-content: center;
    height: 100vh;
  }

  form {
    width: 500px;
    padding: 60px 30px;
  }

  table {
    border-spacing: 10px;
  }

  .d-flex {
    display: flex;
  }

  .py {
    padding: 10px 0;
  }

  .mg {
    margin-right: 20px;
  }

  .w-1 {
    width: 170px;
  }

  .w-3 {
    width: 125px;
  }

  .fl {
    flex: 5;
  }

  .background {
    background-color: rgb(110, 205, 80);
  }

  .text-white {
    color: white;
  }

  .text-center {
    text-align: center;
  }

  .bor_der {
    border: 2px solid rgb(5 85 150);
  }

  .pd {
    padding: 10px 20px;
  }

  .error {
    color: red;
    font-size: 17px;
    margin: 10px 10px 20px 20px;
    display: flex;
  }

  .required_label::after {
    content: " *";
    color: red;
  }

  .form_gr {
    display: flex;
    align-items: center;
    margin-bottom: 12px;
  }

  .form_gr input,
  .form_gr select {
    padding: 10px;
    box-sizing: border-box;
    line-height: 1;
  }

  .button-container {
    text-align: center;
    margin-top: 20px;
  }

  .button-container button {
    padding: 10px 35px;
    background-color: #4caf50;
    color: white;
    cursor: pointer;
    border: 2px solid rgb(5 8 150);
    border-radius: 5px;
  }

  .adress-label {
    height: 41px;
  }

  .adress-input {
    padding: 10px 10px 60px 10px;
  }
</style>

<body>
  <div class="container">
    <form id="registrationForm" class="bor_der">
      <div id="errorMessages" class="error">

        <script>
          $(function () {
            $("#registerButton").click(function (event) {
              event.preventDefault(); // Ngăn chặn việc gửi form mặc định

              var customErrorMessages = [];

              function addCustomErrorMessage(message) {
                customErrorMessages.push(message);
              }

              var name = $("#name").val();
              var gender = $("input[name='gender']:checked").val();
              var department = $("#department").val();
              var birthdate = $("#birthdate").val();

              if (!name) {
                addCustomErrorMessage("Hãy nhập tên.");
              }

              if (!gender) {
                addCustomErrorMessage("Hãy chọn giới tính.");
              }

              if (!department) {
                addCustomErrorMessage("Hãy chọn phân khoa.");
              }

              if (!birthdate) {
                addCustomErrorMessage("Hãy nhập ngày sinh.");
              } else {
                var datePattern = /^\d{2}\/\d{2}\/\d{4}$/;
                if (!datePattern.test(birthdate)) {
                  addCustomErrorMessage("Hãy nhập ngày sinh đúng định dạng dd/mm/yyyy.");
                }
              }

              var errorMessageHtml = customErrorMessages.join("<br>");

              if (customErrorMessages.length > 0) {
                $("#errorMessages").html(errorMessageHtml);
              } else {
                // Nếu không có lỗi, thực hiện đăng ký hoặc xử lý form ở đây
                $("#errorMessages").html(""); // Xóa thông báo lỗi cũ
                // Điều hướng hoặc xử lý form tiếp theo tại đây
              }
            });
          });
        </script>

      </div>

      <div class="form_gr">
        <div class="background text-white bor_der pd w-3 text-center required_label mg" for="name">Họ và tên</div>
        <input class="bor_der fl" type="text" id="name" name="name" required>
      </div>

      <div class="form_gr">
        <div class="background text-white bor_der pd w-3 text-center required_label mg " for="gender">Giới tính</div>
        <div id="gender" name="gender" class="w-1">
          <input type="radio" id="male" name="gender" value="Nam" required> Nam
          <input type="radio" id="female" name="gender" value="Nữ" required> Nữ
        </div>
      </div>

      <div class="form_gr">
        <div class="background text-white bor_der pd w-3 text-center required_label mg" for="department">Phân khoa</div>
        <select id="department" name="department" class="bor_der py">
          <option value="">--Chọn phân khoa--</option>
          <?php
          $departments = array('MAT' => 'Khoa học máy tính', 'KDL' => 'Khoa học vật liệu');
          foreach ($departments as $key => $value) {
            echo "<option value=\"$key\">$value</option>";
          }
          ?>
        </select>
      </div>

      <div class="form_gr">
        <div class="background text-white bor_der pd w-3 text-center required_label mg w-1" for="birthdate">Ngày sinh
        </div>
        <input class="bor_der w-1" type="text" id="birthdate" name="birthdate" placeholder="dd/mm/yyyy" required>
      </div>
      <div class="d-flex">
        <div class="background text-white bor_der pd w-3 text-center mg adress-label" for="adress">Địa chỉ</div>
        <input class="bor_der fl adress-input" type="text" id="adress" name="adress">
      </div>


      <div class="button-container" id="registerButton">
        <button type="submit">Đăng ký</button>
      </div>
    </form>
  </div>

</body>

</html>