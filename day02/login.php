<!DOCTYPE html>
<html lang="vi">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
    <style>
        /* CSS để cải thiện giao diện của trang đăng nhập */

        p {
            background-color: #f2f2f2;
            padding: 8px;
            width: 76%;
            margin-left: 1cm;
        }

        body {
            font-family: Arial, sans-serif;
            background-color: #f7f7f7;
            height: 100vh;
            margin: 0;

        }

        form {
            background-color: #fff;
            padding: 20px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            text-align: left;
            max-width: 50%;
            border: 2px solid #007bff;
            /* Thêm viền xanh nước biển */

        }

        label {
            font-weight: bold;
            background-color: #007bff;
            padding: 10px;
            margin: 8px 0;
            color: white;
            display: inline-block;
            width: 30%;
            margin-right: 0.5cm;
            margin-left: 1cm;
        }

        input[type="text"] {
            width: 39%;
            padding: 10px;
            margin: 8px 0;
            border: 2px solid #007bff;
           

        }

        input[type="password"] {
            width: 39%;
            padding: 10px;
            margin: 8px 0;
            border: 2px solid #007bff;
          
        }


        input[type="submit"] {
            background-color: #007bff;
            color: #fff;
            padding: 10px 40px;
            border: none;
            border-radius: 5px;
            border-style;
            cursor: pointer;
            font-weight: bold;
            margin-top: 10px;
            text-align: center;
            border: 2px solid #007bff;
            display: block;
            /* Để đặt chiều rộng và căn giữa */
            margin: 0 auto;
            /* Căn giữa ngang */
        }
    </style>

</head>

<body>
    <div>
        <form action="process_login.php" method="POST">
            <p>Bây giờ là:
                <?php
                $englishDays = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
                $vietnameseDays = array("Thứ 2 ngày", "Thứ 3 ngày", "Thứ 4 ngày", "Thứ 5 ngày", "Thứ 6 ngày", "Thứ 7 ngày", "Chủ Nhật ngày");

                $date = date("H:i:s, l d/m/Y", time() + 7 + 18000);

                $vietnameseDate = str_replace($englishDays, $vietnameseDays, $date);

                echo $vietnameseDate;
                ?>
            </p>

            <label for="username">Tên đăng nhập:</label>
            <input type="text" id="username" name="username" required><br><br>

            <label for="password">Mật khẩu:</label>
            <input type="password" id="password" name="password" required><br><br>

            <input type="submit" value="Đăng nhập">
        </form>
    </div>
</body>

</html>
