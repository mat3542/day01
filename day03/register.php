<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Đăng ký Tân sinh viên</title>
</head>
<style>
    * {
        box-sizing: border-box;
        border: none;
        outline: unset;
        font-size: 16px;
        font-family: "Times New Roman", Times, serif;
    }

    body {
        display: flex;
        align-items: center;
        justify-content: center;
        height: 100vh;
    }

    form {
        width: 500px;
        padding: 60px 50px;
    }

    table {
        border-spacing: 9px;
    }

    .h-100 {
        height: 90%;
    }

    .d-flex {
        display: flex;
    }

    .py {
        padding: 5px 0;
    }

    .margin-top {
        margin-top: 20px;
    }

    .w-30 {
        width: 30%;
    }

    .w-100 {
        width: 100%;
    }

    .w-30.present-time {
        background-color: rgb(234 229 229);
        margin: 0 0 20px 0;
    }

    .background {
        background-color: rgb(94 171 196);
    }

    .background-green {
        background-color: rgb(86 170 70);
    }

    .text {
        color: white;
    }

    .text-center {
        text-align: center;
    }

    .border {
        border: 2px solid rgb(5 84 149);
    }

    .bt {
        padding: 10px 40px;
        border-radius: 10px;
        cursor: pointer;
    }

    .bt:hover {
        background-color: rgb(135, 211, 237);
    }

    .padding {
        padding: 5px 20px;
    }

    .genders {
        margin-right: 10px;
    }

    .genders input {
        margin-right: 9px;
    }
</style>

<body>
    <form action="" class="border">
        <table class="w-100">
            <tr>
                <td class="background text border padding w-30 text-center">
                    <label for="name">Họ và tên</label>
                </td>
                <td class="border">
                    <input type="text" id="name" name="name" required class="w-100 padding h-100">
                </td>
            </tr>
            <tr>
                <td class="background text border padding w-30 text-center">
                    <label for="gender">Giới tính</label>
                </td>
                <td>
                    <div class="d-flex">
                        <?php
                        $genders = array(0 => 'Nam', 1 => 'Nữ');
                        for ($i = 0; $i < count($genders); $i++) {
                            $key = $i;
                            $value = $genders[$i];

                            echo "<div class='genders'>
                            <input id='$key' type='radio' name='genders' value='$value'>
                            <label for='$key'>$value</label>
                        </div>";
                        }
                        ?>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="background text border padding w-30 text-center">
                    <label for="department">Phân khoa</label>
                </td>
                <td>
                    <select id="department" name="department" class="border py">
                        <option value="">--Chọn phân khoa--</option>
                        <?php
                        $departments = [
                            'MAT' => 'Khoa học máy tính',
                            'KDL' => 'Khoa học vật liệu'
                        ];

                        foreach ($departments as $key => $value) {
                            echo "<option value=\"$key\">$value</option>";
                        }
                        ?>

                    </select>
                </td>
            </tr>
        </table>
        <div class="text-center">
            <button type="submit" class="bt border background-green text margin-top">Đăng ký</button>
        </div>
    </form>
</body>

</html>
