<?php
include("database.php");
$departments = array('MAT' => 'Khoa học máy tính', 'KDL' => 'Khoa học vật liệu');

// Check if the POST variables are set
if (isset($_POST['department']) && isset($_POST['keyword'])) {
    // Prevent SQL injection by using prepared statements
    $stmt = $conn->prepare("SELECT * FROM students WHERE students.department LIKE :department AND students.name LIKE :keyword");
    
    // Bind parameters
    $stmt->bindValue(':department', '%' . $_POST['department'] . '%', PDO::PARAM_STR);
    $stmt->bindValue(':keyword', '%' . $_POST['keyword'] . '%', PDO::PARAM_STR);

    // Execute the query
    $stmt->execute();

    // Fetch the result
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

    // Process the result to generate HTML
    $html = "";
    foreach ($result as $key => $value) {
        $html .= " <tr class='mb-3'>
            <td>" . $value['id'] . "</td>
            <td>" . $value['name'] . "</td>
            <td>" . $departments[$value['department']] . "</td>
            <td class='d-flex justify-content-center'>
                <button class='me-2 db-btn'>Xóa</button>
                <button class='db-btn'>Sửa</button>
            </td>
        </tr>";
    }

    // Return the HTML as JSON
    echo json_encode(
        array(
            "status" => "Success",
            "data" => $html
        )
    );
} else {
    // If POST variables are not set, return an error status
    echo json_encode(array("status" => "Error", "message" => "Invalid request"));
}
?>
