<?php
// Kết nối đến cơ sở dữ liệu
include 'database.php';

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Lấy dữ liệu từ form
    $name = $_POST["name"];
    $gender = $_POST["gender"];
    $department = $_POST['department'];
    $birthdate = $_POST['birthdate'];
    $address = $_POST["address"];

    // Xử lý tệp ảnh
    $targetDir = "uploads/";
    $name_image = basename($_FILES["image"]["name"]);
    $targetFile = $targetDir . $name_image;
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($targetFile, PATHINFO_EXTENSION));

    // Kiểm tra nếu tệp đã tồn tại
    if (file_exists($targetFile)) {
        echo "Sorry, file already exists.";
        $uploadOk = 0;
    }

    // Kiểm tra kích thước tệp
    if ($_FILES["image"]["size"] > 500000) {
        echo "Sorry, your file is too large.";
        $uploadOk = 0;
    }

    // Cho phép các định dạng tệp ảnh nhất định
    if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif") {
        echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
        $uploadOk = 0;
    }

    // Kiểm tra nếu có lỗi khi tải lên
    if ($uploadOk == 0) {
        echo "Sorry, your file was not uploaded.";
    } else {
        // Nếu không có lỗi, thử tải lên tệp
        if (move_uploaded_file($_FILES["image"]["tmp_name"], $targetFile)) {
            // Chuẩn bị truy vấn SQL để chèn dữ liệu vào bảng students với tên tệp ảnh
            $sql = "INSERT INTO students (name, gender, department, birthdate, address, image) 
                    VALUES ('$name', '$gender', '$department', '$birthdate', '$address', '$name_image')";

            // Thực thi truy vấn
            $rs = $conn->exec($sql);

            if ($rs) {
                header("location: index.php");
            } else {
                echo "Error inserting student information.";
            }
        } else {
            echo "Sorry, there was an error uploading your file.";
        }
    }
}
?>
