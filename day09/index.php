<?php
$departments = array('MAT' => 'Khoa học máy tính', 'KDL' => 'Khoa học vật liệu');
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        .transparent-bg {
            background-color: rgba(0, 123, 255, 0.5); /* Màu xanh nước biển trong suốt */
            border: 2px;
            color: white;
        }

        .bold-text {
            font-weight: bold;
        }
    </style>
    <link rel="stylesheet" href="style2.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

</head>

<body>
    <?php
    include "database.php";

    $departmentFilter = isset($_GET['department']) ? $_GET['department'] : '';
    $keywordFilter = isset($_GET['keyword']) ? $_GET['keyword'] : '';

    $stmt = $conn->prepare("SELECT * FROM students");
    $stmt->execute();
    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
    $result = $stmt->fetchAll();
    $rowCount = $stmt->rowCount();
    ?>

    <div class="container mt-4 w-50">
        <div class="row">
            <form class="offset-2" action="">
                <div class="mb-3 row">
                    <label for="department" class="col-2 col-form-label">Khoa</label>
                    <div class="col-4 ps-0">
                        <select id="department" name="department" class="form-control transparent-bg">
                            <option value=""></option>
                            <?php
                            foreach ($departments as $key => $value) {
                                $selected = ($departmentFilter == $key) ? 'selected' : '';
                                echo "<option value=\"$key\" $selected>$value</option>";
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="mb-3 row">
                    <label for="inputKeyword" class="col-2 col-form-label">Từ khóa</label>
                    <div class="col-4 ps-0">
                        <input type="text" class="form-control transparent-bg" id="inputKeyword" name="keyword" value="<?php echo $keywordFilter; ?>">
                    </div>
                </div>
                <div class="row mb-3 ">
                    <button type="submit" class="btn btn-primary offset-2 col-2 find-btn transparent-bg">Tìm kiếm</button>
                </div>
            </form>
        </div>

        <div class="row">
            <p style="padding: 0 8px" id="num_std">
                Số sinh viên tìm thấy: <?php echo count($result) ?>
                <input type="hidden" name="num_student" value="<?php echo count($result) ?>" id="num_student">
            </p>
        </div>

        <div class="d-flex justify-content-end pe-5 ">
            <a href="register3.php">
                <button class="add-btn transparent-bg bold-text">Thêm</button>
            </a>
        </div>

        <div class="row">
            <table class="table">
                <thead>
                    <tr>
                        <td>No</td>
                        <td>Tên sinh viên</td>
                        <td>Khoa</td>
                        <td>
                            <label class="ms-4">action</label>
                        </td>
                    </tr>
                </thead>

                <tbody id="content">
                    <?php
                    foreach ($result as $key => $value) {
                        if (($departmentFilter == '' || $departmentFilter == $value['department']) &&
                            (stripos($value['name'], $keywordFilter) !== false || stripos($value['address'], $keywordFilter) !== false)
                        ) {
                            ?>
                            <tr class="mb-3">
                                <td><?php echo $value['id']; ?></td>
                                <td><?php echo $value['name']; ?></td>
                                <td><?php echo $departments[$value['department']]; ?></td>
                                <td class="d-flex justify-content-center">
                                    <button class="me-2 db-btn transparent-bg bold-text deleteButton">Xóa</button>
                                    <button class="db-btn transparent-bg bold-text editButton">Sửa</button>
                                </td>
                            </tr>
                        <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>

    <!-- Bootstrap and jQuery scripts -->
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js"
        integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"
        integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
    <script>
        $(document).ready(function () {
            $("#department").change(function () {
            searchInfor()
        })

        $("#inputKeyword").keyup(function () {
            searchInfor()
        })

        function searchInfor() {
            var department = $("#department").val()
            var keyword = $("#inputKeyword").val()
            $.ajax({
                url: 'search.php',
                type: "POST",
                data: { department: department, keyword: keyword },
                dataType: 'JSON',
                success: function (response) {
                    if (response.status === "Success") {
                        $("#content").html(response.data)
                    }
                },
                error: function (xhr, status, error) {
                    console.log(xhr.responseText);
                    console.log(status);
                    console.log(error);
                }
            })
        }

        $(".deleteButton").click(function () {
            var confirmation = confirm("Are you sure you want to delete this record?");

            // If the user clicks "Yes" in the confirmation dialog
            if (confirmation) {
                // Perform the delete operation (you may want to implement this using AJAX)
                var row = $(this).closest("tr");
                var id = row.find("td:first").text(); // Assuming the ID is in the first column
                deleteRecord(id, row);

            }
        });

        // Function to delete a record (you can replace this with your actual delete logic)
        function deleteRecord(id, row) {
            
            // Send an AJAX request to delete the record with the specified ID
            $.ajax({
                url: 'delete.php', // Replace with your delete script
                type: 'POST',
                data: { id: id },
                success: function (response) {
                    // Handle the response (e.g., remove the deleted record from the table)
                    row.remove()
                    alert("Record deleted successfully!");
                    $("#num_std").html("Số sinh viên tìm thấy: " + (parseInt($("#num_student").val()) - 1))
                },
                error: function (xhr, status, error) {
                    console.log(xhr.responseText);
                    console.log(status);
                    console.log(error);
                }
            });
        }

        $(".editButton").click(function () {
            // Assuming you have a unique identifier (e.g., student ID) in the first column
            var id = $(this).closest("tr").find("td:first").text();

            // Redirect to update_students.php with the selected ID
            window.location.href = 'update_student.php?id=' + id;
        });
        });
    </script>
</body>

</html>
