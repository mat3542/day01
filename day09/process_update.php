<?php
include "database.php";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Retrieve form data
    $id = $_POST['id'];
    $name = $_POST['name'];
    $gender = $_POST['gender'];
    $department = $_POST['department'];
    $birthdate = $_POST['birthdate'];
    $address = $_POST['address'];
    // Add other form fields as needed

    // Process the uploaded image
    $targetDir = "uploads/";
    $targetFile = $targetDir . basename($_FILES["image"]["name"]);
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($targetFile, PATHINFO_EXTENSION));

    // Check if the image file is a real image or fake image
    $check = getimagesize($_FILES["image"]["tmp_name"]);
    if ($check !== false) {
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
    }

    // Check file size
    if ($_FILES["image"]["size"] > 500000) {
        echo "Sorry, your file is too large.";
        $uploadOk = 0;
    }

    // Allow certain file formats
    if (
        $imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif"
    ) {
        echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
        $uploadOk = 0;
    }

    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        echo "Sorry, your file was not uploaded.";
    } else {
        // If file already exists, overwrite it
        move_uploaded_file($_FILES["image"]["tmp_name"], $targetFile);

        // Update the student information in the database with the image path
        $stmt = $conn->prepare("UPDATE students SET name = :name, gender = :gender, department = :department, birthdate = :birthdate, address = :address, image = :image WHERE id = :id");
        $stmt->bindParam(':name', $name);
        $stmt->bindParam(':gender', $gender);
        $stmt->bindParam(':department', $department);
        $stmt->bindParam(':birthdate', $birthdate);
        $stmt->bindParam(':address', $address);
        $stmt->bindParam(':image', $targetFile);
        $stmt->bindParam(':id', $id);

        if ($stmt->execute()) {
            // Redirect to index.php after successful update
            header("Location: index.php");
            exit();
        } else {
            echo "Error updating student information.";
        }
    }
} else {
    echo "Invalid request.";
}
?>
