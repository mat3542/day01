<?php
include "database.php";

if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['id'])) {
    $id = $_POST['id'];

    $stmt = $conn->prepare("DELETE FROM students WHERE id = :id");
    $stmt->bindParam(':id', $id);
    
    try {
        $stmt->execute();
        echo json_encode(['status' => 'Success']);
    } catch (PDOException $e) {
        // Log or handle the exception as needed
        echo json_encode(['status' => 'Error', 'message' => $e->getMessage()]);
    }
} else {
    echo json_encode(['status' => 'InvalidRequest']);
}
?>
