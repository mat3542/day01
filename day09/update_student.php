<?php
include "database.php";


if (isset($_GET['id'])) {
    $id = $_GET['id'];


    $stmt = $conn->prepare("SELECT * FROM students WHERE id = :id");
    $stmt->bindParam(':id', $id);
    $stmt->execute();
    $student = $stmt->fetch(PDO::FETCH_ASSOC);

    if ($student) {

        ?>
        <!DOCTYPE html>
        <html lang="en">

        <head>
            <title>Cập Nhật Thông Tin Sinh Viên</title>
            <meta name="viewport" content="width=device-width, initial-scale=1.0" />
            <link rel="stylesheet" href="style.css">
            <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        </head>

        <body>
            <form id="updateForm" class="bd-blue" method="POST" action="process_update.php" enctype="multipart/form-data">
                <!-- Thêm các trường mẫu ở đây với các giá trị mặc định từ biến $student -->
                <input type="hidden" name="id" value="<?php echo $student['id']; ?>">
                <div class="form-group">
                    <label class="bg-green text-white bd-blue p-10-20 w-30 text-center  me-20 " for="name">Họ
                        tên:</label>
                    <div class="fl-1 p-10-20">
                        <input type="text" name="name" value="<?php echo $student['name']; ?>" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="bg-green text-white bd-blue p-10-20 w-30 text-center  me-20 " for="gender">Giới tính:</label>
                    <div class="fl-1 p-10-20">
                        <input type="text" name="gender" value="<?php echo $student['gender']; ?>" required>
                    </div>
                </div>

                <div class="form-group">
                    <label class="bg-green text-white bd-blue p-10-20 w-30 text-center  me-20 " for="department">Phân
                        khoa:</label>
                    <div class="fl-1 p-10-20"><select name="department" required>
                            <option value="MAT" <?php echo ($student['department'] == 'MAT') ? 'selected' : ''; ?>>Khoa học máy
                                tính
                            </option>
                            <option value="KDL" <?php echo ($student['department'] == 'KDL') ? 'selected' : ''; ?>>Khoa học vật
                                liệu
                            </option>
                            <!-- Thêm nhiều tùy chọn khác nếu cần dựa trên các giá trị khoa của bạn -->
                        </select> </div>
                </div>

                <div class="form-group">
                    <label class="bg-green text-white bd-blue p-10-20 w-30 text-center me-20 " for="birthdate">Ngày
                        sinh:</label>
                    <div class="fl-1 p-10-20">
                        <input type="text" name="birthdate" value="<?php echo $student['birthdate']; ?>" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="bg-green text-white bd-blue p-10-20 w-30 text-center me-20 " for="address">Địa chỉ:</label>
                    <div class="fl-1 p-10-20">
                        <input type="text" name="address" value="<?php echo $student['address']; ?>" required>
                    </div>
                </div>
                <!-- Thêm trường nhập tệp cho việc tải lên ảnh -->
                <div class="form-group">
                    <label class="bg-green text-white bd-blue p-10-20 w-30 text-center  me-20 " for="image">Ảnh:</label>
                    <div class="fl-1 p-10-20">
                        <input type="file" name="image" accept="image/*">
                    </div>
                </div>
                <div class="button-container" id="registerButton">
                    <button type="submit"> Xác nhận </button>
                </div>

            </form>

        </body>

        </html>
        <?php
    } else {
        echo "Không tìm thấy sinh viên.";
    }
} else {
    echo "Yêu cầu không hợp lệ. Vui lòng cung cấp ID sinh viên.";
}
?>