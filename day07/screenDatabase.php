<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <!-- Link đến CSS của Bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<style>
    .custom {
        background-color: rgba(0, 100, 255, 0.5);
        border: none;
        color: white;
        /* Thay đổi màu chữ thành trắng */
        margin-right: 5px;
    }

    .form-control {
        background-color: rgba(0, 100, 255, 0.2);
        /* Màu xanh trong suốt (ví dụ rgba) */
        border: 1px solid #007BFF;
    }
</style>

<body>
    <?php
    $departments = array('MAT' => 'Khoa học máy tính', 'KDL' => 'Khoa học vật liệu');
    ?>
    <?php
    include "database.php";

    try {
        $stmt = $conn->prepare("SELECT * FROM students");
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        echo "Lỗi: " . $e->getMessage();
    }


    ?>

    <div class="container  mt-5 w-50">
        <div class="row">
            <form class="offset-2" action="">
                <div class="mb-3 row">
                    <label for="department" class="col-2 col-form-label">Khoa</label>
                    <div class="col-4 ps-0">
                        <input type="text" class="form-control " id="department">
                    </div>
                </div>
                <div class="mb-3 row">
                    <label for="inputKeyword" class="col-2 col-form-label">Từ khóa</label>
                    <div class="col-4 ps-0">
                        <input type="text" class="form-control " id="inputKeyword">
                    </div>
                </div>
                <div class="row mb-3 ">
                    <button type="submit" class="btn btn-primary offset-2 col-2">Tìm kiếm</button>
                </div>
            </form>
        </div>
        <div class="row">
            <p style="padding: 0 8px">
                Số sinh viên tìm thấy: xxx
            </p>
        </div>
        <div class="d-flex justify-content-end pe-5">
            <a href="register.php">
                <button class="btn btn-primary" style="background-color: #007BFF; border: none;">Thêm</button>
            </a>
        </div>
        <div class="row">
            <table class="table">
                <tr>
                    <td>No</td>
                    <td>Tên sinh viên</td>
                    <td>Khoa</td>
                    <td>
                        <label class="ms-4">Action</label>
                    </td>
                </tr>
                <?php foreach ($result as $key => $value) { ?>
                    <tr class="mb-3">
                        <td>
                            <?php echo $value['id']; ?>
                        </td>
                        <td>
                            <?php echo $value['name']; ?>
                        </td>
                        <td>
                            <?php echo $departments[$value['department']]; ?>
                        </td>
                        <td class="d-flex justify-content-center">
                            <button class="btn custom">Xóa</button>
                            <button class="btn custom">Sửa</button>
                        </td>
                    </tr>
                <?php } ?>

            </table>
        </div>
    </div>
</body>

</html>