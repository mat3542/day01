<!DOCTYPE html>
<html lang="en">

<head>
    <title>Form Validation</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="style.css">
</head>

<style>
    /* Your CSS styles here */
</style>

<body>
    <div class="container">
        <div id="errorMessages" class="error">
        </div>
        <div class="form_gr">
            <div class="background text-white bor_der pd w-3 text-center required_label mg" for="name">Họ và tên</div>
            <?php
            if (isset($_POST['name'])) {
                echo $_POST['name'];
            }
            ?>
        </div>

        <div class="form_gr">
            <div class="background text-white bor_der pd w-3 text-center required_label mg " for="gender">Giới tính</div>
            <div id="gender" name="gender" class="w-1">
                <?php
                if (isset($_POST['gender'])) {
                    echo $_POST['gender'];
                }
                ?>
            </div>
        </div>

        <div class="form_gr">
            <div class="background text-white bor_der pd w-3 text-center required_label mg" for="department">Phân khoa</div>
            <?php
            if (isset($_POST['department'])) {
                $departments = array('MAT' => 'Khoa học máy tính', 'KDL' => 'Khoa học vật liệu');
                if (isset($departments[$_POST['department']])) {
                    echo $departments[$_POST['department']];
                }
            }
            ?>
        </div>

        <div class="form_gr">
            <div class="background text-white bor_der pd w-3 text-center required_label mg w-1" for="birthdate">Ngày sinh
            </div>
            <?php
            if (isset($_POST['birthdate'])) {
                echo $_POST['birthdate'];
            }
            ?>
        </div>

        <div class="d-flex">
            <div class="background text-white bor_der pd w-3 text-center mg adress-label" for="address">Địa chỉ</div>
            <?php
            if (isset($_POST['address'])) {
                echo $_POST['address'];
            }
            ?>
        </div>

        <div class="d-flex">
            <div class="background text-white bor_der pd w-3 text-center mgin " for="Image" style="height: 10%">Hình ảnh
            </div>

            <?php
            if (isset($_FILES['Image'])) {
                $target_dir = 'uploads/';
                $target_file = $target_dir . basename($_FILES['Image']['name']);
                $maxFileSize = 84031 * 2;
                $fileType = array('jpg', 'png', 'jpeg', 'gif');
                $isImageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
            
                if ($_FILES['Image']['size'] > $maxFileSize) {
                    echo '<div>Kích thước của ảnh quá lớn</div>';
                } elseif (!in_array($isImageFileType, $fileType)) {
                    echo '<div>Sai định dạng ảnh</div>';
                } elseif (move_uploaded_file($_FILES['Image']['tmp_name'], $target_file)) {
                    echo '<img src="./uploads/' . $_FILES["Image"]['name'] . '" alt="" style="width:50%; margin-left: 15px;">';
                } else {
                    echo 'Không thể upload file';
                }
            }            
            ?>
        </div>
    </div>
</body>
</html>
